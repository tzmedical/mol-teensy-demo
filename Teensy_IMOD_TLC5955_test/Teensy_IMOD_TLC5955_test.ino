/*
   TLC5955 Example Program
   Zack Phillips, zkphil@berkeley.edu
   Updated 11/16/2015
*/

#include "tz_TLC5955.h"
#include <SPI.h>
#include <Bounce2.h>

TLC5955 tlc;

// Define this to allow the upper button to control the mode
#define SECRET_5

// Define this to choose which version of the LED ring is being used
#define IMOD_LED_RING_REV 2

#define GSCLK 10 // On Teensy 3.2
#define LAT 9   // On Teensy 3.2

//#define GSCLK 13 // On Uno
//#define LAT 12   // On Uno

// Spi pins are needed to send out control bit (SPI only supports bytes)
#define SPI_MOSI 11 // 11 on Teensy 3.2 and Uno
#define SPI_CLK 13 // 13 on Teensy 3.2 and Uno

#define BOT_BUTTON_PIN 14 // PB2 -> 14 on Teensy 3.2 
#define TOP_BUTTON_PIN 15 // PB1 -> 15 on Teensy 3.2
//#define BOT_BUTTON_PIN 23 // PB2 -> 14 on Uno
//#define TOP_BUTTON_PIN 24 // PB1 -> 15 on Uno


#define MAX_ANGLE 360

#define CW 0
#define CCW 1

#ifdef SECRET_5
enum {
  DEMO_MODE = 0,
  CHASER_MODE = 1,
  RING_MODE = 2,
  SHIFTER_MODE = 3,
  BOUNCE_MODE = 4,
  NUM_MODES = 5
};
#endif

#if IMOD_LED_RING_REV == 1
#define NUM_LEDS  48
#define RGB_LEDS  0
static const uint8_t led_lookup[NUM_LEDS + RGB_LEDS] = {
  [0] = 0, [1] = 1, [2] = 2,
  [3] = 3, [4] = 4, [5] = 5,
  [6] = 6, [7] = 7, [8] = 8,
  [9] = 9, [10] = 10, [11] = 11,
  
  [12] = 12, [13] = 22, [14] = 20,
  [15] = 17, [16] = 15, [17] = 13,
  [18] = 14, [19] = 16, [20] = 18,
  [21] = 19, [22] = 21, [23] = 23,
  
  [24] = 24, [25] = 25, [26] = 26,
  [27] = 27, [28] = 28, [29] = 30,
  [30] = 32, [31] = 34, [32] = 33,
  [33] = 31, [34] = 29, [35] = 35,
 
  [36] = 36, [37] = 37, [38] = 38,
  [39] = 39, [40] = 40, [41] = 41,
  [42] = 43, [43] = 45, [44] = 46,
  [45] = 44, [46] = 42, [47] = 47,
};
#endif

#if IMOD_LED_RING_REV == 2
#define NUM_LEDS  45
#define RGB_LEDS  3
static const uint8_t led_lookup[NUM_LEDS + RGB_LEDS] = {
  [0] = 0, [1] = 1, [2] = 17,
  [3] = 15, [4] = 16, [5] = 5,
  [6] = 3, [7] = 4, [8] = 8,
  [9] = 6, [10] = 7, [11] = 20,
  
  [12] = 18, [13] = 19, [14] = 11,
  [15] = 9, [16] = 10, [17] = 23,
  [18] = 21, [19] = 22, [20] = 34,
  [21] = 33, [22] = 35, [23] = 46,
  
  [24] = 45, [25] = 47, [26] = 31,
  [27] = 30, [28] = 32, [29] = 43,
  [30] = 42, [31] = 44, [32] = 40,
  [33] = 39, [34] = 41, [35] = 28,
 
  [36] = 27, [37] = 29, [38] = 37,
  [39] = 36, [40] = 38, [41] = 25,
  [42] = 24, [43] = 26, [44] = 14,
  [45] = 12, [46] = 13, [47] = 2,
};
#endif

#ifdef SECRET_5
uint8_t headLed; // Used for eye candy function
uint8_t currentLed; // Used for eye candy function
uint8_t startLed; // Used for eye candy function
uint8_t extending; // Used for eye candy function
uint8_t currentDir; // Used for eye candy function
uint8_t currentLevel; // Used for eye candy function
uint8_t currentMode;
#endif

uint8_t signalStrength;
uint8_t signalGrowth;
uint16_t mAngle;

// Instantiate a Bounce object
Bounce debouncer = Bounce();
Bounce modeButton = Bounce(); 

void setup() {
  // Now set the GSCKGRB to an output and a 50% PWM duty-cycle
  // For simplicity all three grayscale clocks are tied to the same pin
  pinMode(GSCLK, OUTPUT);
  pinMode(LAT, OUTPUT);

  // Setup the buttons with internal pull-up :
  pinMode(BOT_BUTTON_PIN,INPUT_PULLUP);
  pinMode(TOP_BUTTON_PIN,INPUT_PULLUP);

  // After setting up the button, setup the Bounce instance :
  debouncer.attach(BOT_BUTTON_PIN);
  debouncer.interval(5); // interval in ms

  modeButton.attach(TOP_BUTTON_PIN);
  modeButton.interval(5); // interval is ms
  
  // Adjust PWM timer (Specific to each microcontroller)
//  TCCR2B = TCCR2B & 0b11111000 | 0x01;    // Arduino Uno?
  analogWriteFrequency(GSCLK, 375000);  // Teensy 3.1 / 3.2

  // Set up clock pulse
  analogWrite(GSCLK, 127);

  // The library does not ininiate SPI for you, so as to prevent issues with other SPI libraries
  SPI.begin();

  // init(GSLAT pin, XBLNK pin, default grayscale values for all LEDS)
  tlc.init(LAT, SPI_MOSI, SPI_CLK);

  // We must set dot correction values, so set them all to the brightest adjustment (0-127)
  tlc.setAllDcData(127); // (range: 0-127)

  // Set Max Current Values (see TLC5955 datasheet)
  tlc.setMaxCurrent(4);

  // Set Function Control Data Latch values. See the TLC5955 Datasheet for the purpose of this latch.
  //tlc.setFunctionData(false, true, true, false, true); // WORKS with fast update
  tlc.setFunctionData(true, false, false, true, true);   // WORKS generally

  // set all brightness levels to max (127)
  int currentVal = 30;
  tlc.setBrightnessCurrent(currentVal);

  // Update Control Register
  tlc.updateControl();

  // Update the GS register (ideally LEDs should be dark up to here)
  tlc.setAllLed(0);
  tlc.updateLeds();

#ifdef SECRET_5
  currentMode = BOUNCE_MODE;
  signalStrength = 0;
  signalGrowth = 0;
  mAngle = 0;

  headLed = 0;
  currentLed = headLed;
  startLed = headLed;
  extending = 1;
  currentLevel = 1;
  currentDir = CW;
#endif
}

void loop() {
  uint16_t mDelay = 100;
  uint16_t mDelayBetween = 100;
  
  int8_t mAngleOffset;
  
  
   
  // Update the Bounce instance :
  debouncer.update();
  modeButton.update();

#ifdef SECRET_5
  uint8_t leng; //Used for eye candy
  uint8_t num;
  if (modeButton.fell())
  {
    currentMode = (currentMode + 1) % NUM_MODES;
    currentLed = headLed;
    startLed = headLed;
    extending = 1;
    currentLevel = 1;
  }
#endif

  // Get the updated value :
  uint8_t buttonPressed = debouncer.fell();
  uint8_t buttonReleased = debouncer.rose();
  uint8_t buttonHeld = debouncer.read();  
#ifdef SECRET_5
  switch (currentMode) {
    case DEMO_MODE:
#endif

      if (buttonPressed)
      {
        signalStrength = 1;
        signalGrowth = 1;
        mAngle = random(360);
        setVector(mAngle, signalStrength);
      }
      if (modeButton.read() == LOW)
      {
        if (signalStrength > 0)
        {
          if ((signalStrength >= ((signalGrowth / 5) + 1)) || (signalStrength >= 6))
          {
            if (signalGrowth < 25)
              signalStrength -= ((signalGrowth / 5) + 1);
            else
              signalStrength -= 6;
          }
          else
          {
            mAngle = random(360);
            signalStrength = 0;
          }
          if (signalGrowth > 0) signalGrowth--;
          mAngleOffset = random(5) - 2;
          delay(mDelay);
          mAngle = (mAngle + mAngleOffset + 360) % 360;
          setVector(mAngle, signalStrength);
        }
      }
      else if (buttonHeld == LOW)
      {
        if (signalStrength < 200)
        {
          if (signalGrowth < 25)
            signalStrength += ((signalGrowth / 5) + 1);
          else
            signalStrength += 6;
          signalGrowth++;
          mAngleOffset = random(5) - 2;
          delay(mDelay);
          mAngle = (mAngle + mAngleOffset + 360) % 360;
          setVector(mAngle, signalStrength);
        }
      }
      else
      {
        // Turn off the display
        setVector(0, 0);
      }

#ifdef SECRET_5
    break;
    case CHASER_MODE:
      leng = 8;
      if (buttonPressed)
      {
        currentLed = headLed;
        extending = 1;
        currentDir = CCW - currentDir;
      }
      chaser(leng, currentDir, 10, 100);
    break;
    case RING_MODE:
      leng = NUM_LEDS;
      if (buttonPressed)
      {
        currentDir = CCW - currentDir;
      }
      ring(leng, currentDir, 10);
    break;
    case SHIFTER_MODE:
#if IMOD_LED_RING_REV == 1
      leng = 16;
#elif IMOD_LED_RING_REV == 2
      leng = 15;
#endif
      if (buttonPressed)
      {
        currentLed = headLed;
        extending = 1;
        currentDir = CCW - currentDir;
      }
      shifter(leng, currentDir, 30, 0, 10000);
    break;
    case BOUNCE_MODE:
      num = 1;
      if (buttonPressed)
      {
        startLed = headLed;
        currentLed = headLed;
        extending = 1;
      }
      led_bounce(num, currentDir, 3, 0, 10000);
    break;
    default:
      currentMode = DEMO_MODE;
    break;
  }  
#endif
}

void setVector(uint16_t angle, uint8_t strength)
{
  uint16_t dimMax = 60000;
  uint16_t dimMin = 100;
  uint16_t dimVal;
  uint8_t centerLed = ((angle * NUM_LEDS) + (MAX_ANGLE >> 1)) / MAX_ANGLE;
  uint8_t numLedOn = 0;
  uint8_t numLedBright = 0;
  
  // Signal Strength is from 0 to 200
  if (strength > 200) strength = 200;

  if (strength < 100)
  {
    numLedOn = ((strength * NUM_LEDS) + 50) / 100;
    numLedBright = 0;
  }
  else
  {
    numLedOn = NUM_LEDS;
    numLedBright = (((strength - 100) * (NUM_LEDS + 1)) + 50) / 100;
  }  

  tlc.setAllLed(0);
  if (numLedOn)
  {
    dimVal = dimMax;
    if (1 == numLedOn)
    {
      dimVal = (dimMax + 1) >> 1;
      if (dimVal < dimMin) dimVal = dimMin;
    }
    tlc.setLed(led_lookup[centerLed], dimVal);

    if (numLedBright)
    {
      if (numLedBright < 10)
        dimVal = (dimMax + (numLedBright >> 1)) / numLedBright;
      else
        dimVal = (dimMax + 5) / 10;
      if (dimVal < dimMin) dimVal = dimMin << 1;
      
      for (uint8_t i = 0; i < (((numLedBright + 1) / 2)); i++)
      {       
        tlc.setLed(led_lookup[(centerLed + i) % NUM_LEDS], dimVal);
        tlc.setLed(led_lookup[(centerLed + NUM_LEDS - i) % NUM_LEDS], dimVal);
      }
    }
    for (uint8_t i = (((numLedBright + 1) / 2)); i <= ((numLedOn + 1) / 2); i++)
    {
      dimVal = (dimVal + 1) >> 1;
      if (dimVal < dimMin) dimVal = dimMin;
      tlc.setLed(led_lookup[(centerLed + i) % NUM_LEDS], dimVal);
      tlc.setLed(led_lookup[(centerLed + NUM_LEDS - i) % NUM_LEDS], dimVal);
    }  
  }
  else if (strength)
  {
    dimVal = dimMin;
    tlc.setLed(led_lookup[centerLed], dimVal);
  }
  tlc.updateLeds();
}

#ifdef SECRET_5
//=========================================================================
void chaser(uint8_t dist, uint8_t dir, uint16_t extendDelay, uint16_t pauseDelay)
{
  uint8_t leng = 0;
  
  if (extending)
  { 
    if (CW == dir)
      headLed = (headLed + 1) % NUM_LEDS;  
    else
      headLed = (headLed - 1 + NUM_LEDS) % NUM_LEDS;
  }
  else
  {
    if (CW == dir)
      currentLed = (currentLed + 1) % NUM_LEDS;
    else
      currentLed = (currentLed - 1 + NUM_LEDS) % NUM_LEDS;
  }
  
  if (CW == dir)
    leng = (headLed - currentLed + NUM_LEDS + 1) % NUM_LEDS;
  else
    leng = (currentLed - headLed + NUM_LEDS + 1) % NUM_LEDS;

  // Draw the arc
  tlc.setAllLed(0);
  arc(headLed, leng, dir, 30000);  
  delay(extendDelay);

  if (dist == leng)
  {
    extending = 0;
    delay(pauseDelay);
  }
  else if (1 == leng)
  {
    extending = 1;
    delay(pauseDelay);
  }
}

//=========================================================================
void shifter(uint8_t dist, uint8_t dir, uint16_t extendDelay, uint16_t pauseDelay, uint16_t led_brightness)
{
  uint8_t leng = 0;
  
  if (extending)
  { 
    if (CW == dir)
      headLed = (headLed + 1) % NUM_LEDS;  
    else
      headLed = (headLed - 1 + NUM_LEDS) % NUM_LEDS;
  }
  else
  {
    if (CW == dir)
      currentLed = (currentLed + 1) % NUM_LEDS;
    else
      currentLed = (currentLed - 1 + NUM_LEDS) % NUM_LEDS;
  }
  
  if (CW == dir)
    leng = (headLed - currentLed + NUM_LEDS + 1) % NUM_LEDS;
  else
    leng = (currentLed - headLed + NUM_LEDS + 1) % NUM_LEDS;

  // Draw the arcs
  tlc.setAllLed(0);
  for (int i = 0; i < NUM_LEDS / dist; i++)
  {
    arc(headLed + (i * dist), leng, dir, led_brightness);
  }
  delay(extendDelay);

  if (dist == leng)
  {
    extending = 0;
    delay(pauseDelay);
  }
  else if (1 == leng)
  {
    extending = 1;
    delay(pauseDelay);
  }
}

//=========================================================================
void led_bounce(uint8_t num, uint8_t dir, uint16_t extendDelay, uint16_t pauseDelay, uint16_t led_brightness)
{ 
  uint8_t leng = 0;
  uint8_t dist = ((NUM_LEDS + num) / (num * 2)) - currentLevel;
  
  if (extending)
  { 
    if (CW == dir)
      headLed = (headLed + 1) % NUM_LEDS;  
    else
      headLed = (headLed - 1 + NUM_LEDS) % NUM_LEDS;
  }
  else
  {
    if (CW == dir)
      currentLed = (currentLed + 1) % NUM_LEDS;
    else
      currentLed = (currentLed - 1 + NUM_LEDS) % NUM_LEDS;
  }
  
  if (CW == dir)
    leng = (headLed - currentLed + NUM_LEDS + 1) % NUM_LEDS;
  else
    leng = (currentLed - headLed + NUM_LEDS + 1) % NUM_LEDS;

  //if (leng > 5) leng = 5;

  // Draw the arcs
  tlc.setAllLed(0);
  arc(headLed, leng, dir, led_brightness);
  arc((startLed - headLed + NUM_LEDS) % NUM_LEDS, leng, CCW - dir, led_brightness);
  
  delay((extendDelay * ((NUM_LEDS + num) / (num * 2))) / dist);

  if (dist == leng)
  {
    extending = 0;
    delay(pauseDelay);
  }
  else if (1 == leng)
  {
    extending = 1;
    currentLevel++;
    currentDir = CCW - currentDir;
    delay(pauseDelay);
  }

  if (dist < 2)
  {
    // Reset everything 
    extending = 1;
    currentLevel = 1;
    headLed = startLed;
    currentLed = headLed;
  }
}

//=========================================================================
void ring(uint8_t dist, uint8_t dir, uint16_t extendDelay)
{
  if (CW == dir)
    headLed = (headLed + 1) % NUM_LEDS;  
  else
    headLed = (headLed - 1 + NUM_LEDS) % NUM_LEDS;
  tlc.setAllLed(0);
  arc(headLed, dist, dir, 30000);
  delay(extendDelay);
}

//=========================================================================
void arc(uint8_t head, uint8_t leng, uint8_t dir, uint16_t dimMax)
{
  uint16_t dimMin = 1000;
  uint16_t dimVal = dimMax;
  if (CW == dir)
  {
    for (uint8_t i = 0; i < leng; i++)
    {
      tlc.setLed(led_lookup[(head - i + NUM_LEDS)%NUM_LEDS], dimVal);
      dimVal = (dimVal + 1) >> 1;
      if (dimVal < dimMin) dimVal = dimMin;
    }
  }
  else if (CCW == dir)
  {
    for (uint8_t i = 0; i < leng; i++)
    {
      tlc.setLed(led_lookup[(head + i)%NUM_LEDS], dimVal);
      dimVal = (dimVal + 1) >> 1;
      if (dimVal < dimMin) dimVal = dimMin;
    }
  }
  
  tlc.updateLeds();
}
#endif
